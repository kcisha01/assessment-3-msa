#include"Mongoose.h"
#include<string>
#include<iostream>
using namespace std;



static struct mg_serve_http_opts s_http_server_opts;

static void ev_handler(struct mg_connection *connection, int ev, void* p) {
	if (ev == MG_EV_HTTP_REQUEST)
		mg_serve_http(connection,(struct http_message *)p, s_http_server_opts);
}

int initserver(int port);
int main()
{
	int port=2020;
	
	initserver(port);

	return 0;
}
int initserver(int port)
{
	//mongoose event manager
	struct mg_mgr manager;

	//mogooose connection
	struct mg_connection* connection;
	string characterport = to_string(port);
	static char const * statport = characterport.c_str();

	mg_mgr_init(&manager, NULL);
	cout << "Starting web server on port:" << statport << endl;
	connection = mg_bind(&manager, statport,ev_handler);

	if (connection == NULL)
	{
		cout << "Failed to create the listener" << endl;
		return 1;
	}
	mg_set_protocol_http_websocket(connection);
	s_http_server_opts.document_root = ".";
	
	while (true)
	{
		mg_mgr_poll(&manager, 200);

	}
	mg_mgr_free(&manager);
	return 0;
}